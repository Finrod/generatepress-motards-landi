// Copy folders and files
module.exports = function(grunt) {
	"use strict";
	return {
		project: {
			files: [{
				expand: true,
				cwd: 'src/',
				src: [
					"screenshot.png",
					"img/**"
				],
				dest: 'dist/'
			}]
		}
	};
};

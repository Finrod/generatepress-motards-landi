// Upload on FTP
module.exports = {
	all: {
		options: {
			authKey: "FtpMotards",
			host: "lesmotardsdupaysdelandi.fr",
			dest: "/public_html/wp-content/themes/generatepressmotardslandi",
			port: 21
		},
		files: [{
			expand: true,
			cwd: 'dist/',
			src: ["**/*"]
		}]
	}
};

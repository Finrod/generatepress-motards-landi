// Remove local route rewrite in .htaccess and activate prod route
module.exports = {
	version: {
		src: ['dist/style.css'],
		overwrite: true,
		replacements: [{
			from: '#VERSION#',
			to: '<%= package.version %>'
		}]
	}
};

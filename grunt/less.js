// Compile LESS into CSS
module.exports = {
	compile: {
		files: {
			"dist/style.css": "src/less/style.less"
		}
	}
};

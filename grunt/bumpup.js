// Increment version number
module.exports = {
	options: {
		dateformat: 'YYYY-MM-DD HH:mm:ss',
		updateProps: {
            package: 'package.json'
        }
	},
	files: ['package.json']
};
